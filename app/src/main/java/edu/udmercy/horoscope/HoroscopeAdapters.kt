package edu.udmercy.horoscope

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.list_item.view.*

class HoroscopeAdapters(val items : ArrayList<String>)
    : RecyclerView.Adapter<HoroscopeAdapters.ViewHolder>() {

    inner class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {
        // Holds the TextView that will add each horoscope to
        val tvHoroType = view.horo_type
    }
    // Inflates the item views
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HoroscopeAdapters.ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_item, parent, false))
    }

    // Binds each horoscope in the ArrayList to a view
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder?.tvHoroType?.text = items.get(position)
    }

    // Gets the number of horoscopes in the list
    override fun getItemCount(): Int {
        return items.size
    }
}
