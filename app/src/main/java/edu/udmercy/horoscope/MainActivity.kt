package edu.udmercy.horoscope

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    // Initializing an empty ArrayList to be filled with horoscopes

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val horoscopes: ArrayList<String> = ArrayList()
        horoscopes.add("Aries")
        horoscopes.add("Taurus")
        horoscopes.add("Gemini")
        horoscopes.add("Cancer")
        horoscopes.add("Leo")
        horoscopes.add("Virgo")
        horoscopes.add("Libra")
        horoscopes.add("Scorpio")
        horoscopes.add("Sagittarius")
        horoscopes.add("Capricorn")
        horoscopes.add("Aquarius")
        horoscopes.add("Pisces")

        // gdn
        val recyclerView = findViewById(R.id.horo_list_item) as RecyclerView

        // Creates a vertical Layout Manager
        // horo_list_item.layoutManager = LinearLayoutManager(this)
        // gdn
        recyclerView.layoutManager = LinearLayoutManager(this)

        // You can use GridLayoutManager if you want multiple columns. Enter the number of columns as a parameter.
//        horo_list_item.layoutManager = GridLayoutManager(this, 2)cd

        // Access the RecyclerView Adapter and load the data into it
        horo_list_item.adapter = HoroscopeAdapters(horoscopes)

    }

}